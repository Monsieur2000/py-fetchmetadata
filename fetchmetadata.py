#!./.venv/bin/python
# -*- coding: utf-8 -*-

# TODO mettre un décompte des entrée mises à jour et ajoutées dans la BD
# TODO améliorer le script, il y a beaucoup de code redondant

from pymongo import MongoClient
import argparse
import getpass
import time
from pathlib import Path
from exiftool import ExifToolHelper
import os
import math
import datetime
from dotenv import load_dotenv
import logging

class mydict(dict):
    """ Classe pour retrouver des infos dans les metatdatas avec des noms de champs partiels
        Car certain champs sont préfixés avec des termes comme QuickTime ou pas
    """
    def __getitem__(self, value):
        keys = [k for k in self.keys() if value in k]
        key = keys[0] if keys else None
        return self.get(key)

def convert_size(size_bytes):
    # FIXME mieux vaudrait tout mettre dans une seule unité, ce serait mieux
    # pour calculer des sommes dans la base de données
    """ Pour convertir les Bytes en unités plus grandes """
    if size_bytes == 0:
        return "0B"
    size_name = ("B", "kB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB")
    i = int(math.floor(math.log(size_bytes, 1024)))
    p = math.pow(1024, i)
    s = round(size_bytes / p, 1)
    return (s, size_name[i])

######################
# variables globales #
######################
base_path = "/mnt/nas_archives"
nb_fichiers_existants = nb_fichiers_new = 0
chemins = (f"{base_path}/pieces/diffusion", f"{base_path}/pieces/mezzanine", f"{base_path}/pieces/preservation")

def main(args):
    # Chargement des infos du .env
    load_dotenv()
    DB_USER = os.getenv('DB_USER')
    DB_PWD = os.getenv('DB_PWD')

    # config du loggeur
    logging.basicConfig(
        level=logging.INFO,
        format="%(asctime)s %(levelname)s %(message)s",
        datefmt="%Y-%m-%d %H:%M:%S",
        handlers=[
            logging.FileHandler("fetchmetadata.log"),
            logging.StreamHandler()
        ]
    )

    connect_db(DB_USER, DB_PWD)

    start = time.time()

    checkFiles(chemins,args.c,fetchCheminsExistants(args.c))

    resultats(start)


def connect_db(user=None, pwd=None):
    global docs_num

    # Récupération du login de connection à la base de donnée
    if not user:
        user = input("MongoDB user : ")

    if not pwd:
        pwd = getpass.getpass(prompt="MongoDB password : ")

    # Création du client de la base Mongo
    mClient = MongoClient(
        host="130.223.28.115",
        port=27017,
        username=user,
        password=pwd,
        authSource="admin"
    )

    db = mClient['fjme-db']

    docs_num = db.objets

def fetchCheminsExistants(cote):
    regexp = "^"+cote+"-"
    query = {"cote" : {"$regex" : regexp}, "objecttype" : "digital"}
    projection = {"_id" : 0, "localisation" : 1}
    localisations = docs_num.find(query, projection=projection)

    list_loc = []
    for l in localisations:
        list_loc.append(base_path + l['localisation'])

    return list_loc

def fetchVideoMetadata(chemin, new=False):
    # récupération des métadonées
    with ExifToolHelper() as et:
        d = et.get_metadata(chemin, params=["-api","largefilesupport=1"])

    # création du dictionnaire pour réupérer les infos plus facilement
    my_dict = mydict(d[0])

    # récup des info de nom du fichier
    v_path, v_filename = os.path.split(chemin)
    v_name, v_ext = os.path.splitext(v_filename)

    # manipulations pour créer les champs de la base de données
    cote = v_name.split('_')[0]

    try:
        duree = math.ceil(my_dict['Duration'] / 60)
    except:
        duree = None

    encodage = [my_dict['CompressorID'], my_dict['AudioFormat']]

    taille = {}
    taille['hauteur'] = {'val' : my_dict['ImageHeight'], 'unite' : 'px'}
    taille['largeur'] = {'val' : my_dict['ImageWidth'], 'unite' : 'px' }

    v_volume, v_unite = convert_size(my_dict['FileSize'])
    taille['volume'] = {'val' : float(v_volume), 'unite' : v_unite}

    echantillonnage = my_dict['AudioSampleRate']

    if my_dict['FileType']:
        extension = my_dict["FileType"].lower()
    else:
        extension = None

    # construction des champs à poster dans la BD
    data = {}
    data['cote'] = cote
    data['duree'] = duree
    data['encodage'] = encodage
    data['taille'] = taille
    data['echantillonnage'] = echantillonnage
    data['extension'] = extension
    data['filename'] = v_filename
    data['localisation'] = str(chemin).replace(base_path, '')
    data['mime'] = my_dict['MIMEType']
    data['objecttype'] = 'digital'
    data['qte'] = 1
    data['lastModif'] = datetime.datetime.now(datetime.UTC)
    if new:
        data['dateCreation'] = datetime.datetime.now(datetime.UTC)

    # envoi de la requête
    # on utilise la fonction replace avec upsert=True pour le cas où on voudrait faire des updates de la base. On se base sur le champs localisation car c'est l'URI du fichier en question. Elle est donc unique pour chaque entrée, contrairement à la cote ou même le filename.
    x = docs_num.replace_one({"localisation" : data['localisation']}, data, upsert=True)

    if x.modified_count:
        logging.info(f"{v_filename} modifiée")
    else:
        logging.info(f"{v_filename} ajoutée dans la base")

def fetchImageMetadata(chemin, new=False):
    # récupération des métadonées
    with ExifToolHelper() as et:
        d = et.get_metadata(chemin, params=["-api","largefilesupport=1"])

    # création du dictionnaire pour réupérer les infos plus facilement
    my_dict = mydict(d[0])

    # récup des info de nom du fichier
    i_path, i_filename = os.path.split(chemin)
    i_name, i_ext = os.path.splitext(i_filename)

    # manipulations pour créer les champs de la base de données
    cote = i_name.split('_')[0]

    encodage = my_dict['FileType']

    taille = {}
    resolution = None
    if encodage != 'PDF':
        taille['hauteur'] = {'val' : my_dict['ImageHeight'], 'unite' : 'px'}
        taille['largeur'] = {'val' : my_dict['ImageWidth'], 'unite' : 'px' }

        resolution = my_dict['XResolution']

    i_volume, i_unite = convert_size(my_dict['FileSize'])
    taille['volume'] = {'val' : float(i_volume), 'unite' : i_unite}

    if my_dict['FileType']:
        extension = my_dict["FileTypeExtension"].lower()
    else:
        extension = None

    # construction des champs à poster dans la BD
    data = {}
    data['cote'] = cote
    data['encodage'] = encodage
    data['taille'] = taille
    data['extension'] = extension
    data['filename'] = i_filename
    data['localisation'] = str(chemin).replace(base_path, '')
    data['mime'] = my_dict['MIMEType']
    data['objecttype'] = 'digital'
    data['qte'] = 1
    data['lastModif'] = datetime.datetime.now(datetime.UTC)
    if resolution: data['resolution'] = resolution
    if new:
        data['dateCreation'] = datetime.datetime.now(datetime.UTC)

    # envoi de la requête
    # on utilise la fonction replace avec upsert=True pour le cas où on voudrait faire des updates de la base. On se base sur le champs localisation car c'est l'URI du fichier en question. Elle est donc unique pour chaque entrée, contrairement à la cote ou même le filename.
    x = docs_num.replace_one({"localisation" : data['localisation']}, data, upsert=True)

    if x.modified_count:
        logging.info(f"{i_filename} modifiée")
    else:
        logging.info(f"{i_filename} ajoutée dans la base")

def fetchAudioMetadata(chemin, new=False):
    # récupération des métadonées
    with ExifToolHelper(common_args=None) as et: # on mets common_args=None sinon Exiftool est lancé avec -G et -n qui ne donne pas la fréquence d'échantillonage
        d = et.get_metadata(chemin, params=["-api","largefilesupport=1"])

    # création du dictionnaire pour réupérer les infos plus facilement
    my_dict = mydict(d[0])

    # récup des info de nom du fichier
    a_path, a_filename = os.path.split(chemin)
    a_name, a_ext = os.path.splitext(a_filename)

    # manipulations pour créer les champs de la base de données
    cote = a_name.split('_')[0]

    encodage = my_dict['FileType']

    try:
        duree = math.ceil(my_dict['Duration'] / 60)
    except:
        h, m, _ = my_dict['Duration'].split(':')
        duree = 60*int(h) + int(m) # on s'en fout de secondes

    echantillonnage = my_dict['SampleRate']
    # FIXME exiftool pour les mp3 donne un Sample Rate de 0 avec l'option -n (qui est utilisée par défault par la lib python)
    # mais sans le -n il donne un sample rate de 44100 !

    taille = {}

    a_volume, a_unite = my_dict['FileSize'].split() # convert_size(my_dict['FileSize'])
    taille['volume'] = {'val' : float(a_volume), 'unite' : a_unite}

    if my_dict['FileType']:
        extension = my_dict["FileTypeExtension"].lower()
    else:
        extension = None

    # construction des champs à poster dans la BD
    data = {}
    data['cote'] = cote
    data['encodage'] = encodage
    data['taille'] = taille
    data['extension'] = extension
    data['filename'] = a_filename
    data['localisation'] = str(chemin).replace(base_path, '')
    data['mime'] = my_dict['MIMEType']
    data['echantillonnage'] = echantillonnage
    data['duree'] = duree
    data['objecttype'] = 'digital'
    data['qte'] = 1
    data['lastModif'] = datetime.datetime.now(datetime.UTC)
    if new:
        data['dateCreation'] = datetime.datetime.now(datetime.UTC)

    # envoi de la requête
    # on utilise la fonction replace avec upsert=True pour le cas où on voudrait faire des updates de la base. On se base sur le champs localisation car c'est l'URI du fichier en question. Elle est donc unique pour chaque entrée, contrairement à la cote ou même le filename.
    x = docs_num.replace_one({"localisation" : data['localisation']}, data, upsert=True)

    if x.modified_count:
        logging.info(f"{a_filename} modifiée")
    else:
        logging.info(f"{a_filename} ajoutée dans la base")


def checkFiles(chemins, cote, existants):
    """ Récupère la liste des fichiers sur le disque, vérifie si ils sont dans la base_path
        de données et lance le récupérateur de métadonnées sinon"""

    global nb_fichiers_new
    global nb_fichiers_existants
    # liste des extension de fichier à prendre en compte
    # c'est pour prévenir des éventuels format pas encore compatible avec les récupérateur de métadonnées
    v_exts = [".mov",".mkv",".mp4",".dv",".avi",".webm"]
    i_exts = [".jpg", ".pdf", ".tif", ".tif"]
    a_exts = [".mp3", ".aiff", ".wav", ".wma"]

    for c in chemins:

        v_chemin = c+"/video/"+cote
        i_chemin = c+"/image/"+cote
        a_chemin = c+"/audio/"+cote

        # Check des vidéos
        if os.path.isdir(v_chemin):
            videos = []
            videos = [p for p in Path(v_chemin).iterdir() if p.suffix in v_exts]

            for v in videos:
                if str(v) in existants:
                    logging.debug(f"EXISTE : {v}")
                    nb_fichiers_existants += 1
                    if args.u is not None and args.u in str(v):
                        fetchVideoMetadata(v)
                else:
                    logging.debug(f"NEW : {v}")
                    nb_fichiers_new += 1
                    fetchVideoMetadata(v,True)

        # Check des images
        if os.path.isdir(i_chemin):
            images = []
            images = [p for p in Path(i_chemin).iterdir() if p.suffix in i_exts]

            for i in images:
                if str(i) in existants:
                    logging.debug(f"EXISTE : {i}")
                    nb_fichiers_existants += 1
                    if args.u is not None and args.u in str(i):
                        fetchImageMetadata(i)
                else:
                    logging.debug(f"NEW : {i}")
                    nb_fichiers_new += 1
                    fetchImageMetadata(i, True)

        # Check des sons
        if os.path.isdir(a_chemin):
            audios = []
            audios = [p for p in Path(a_chemin).iterdir() if p.suffix in a_exts]

            for a in audios:
                if str(a) in existants:
                    logging.debug(f"EXISTE : {a}")
                    nb_fichiers_existants += 1
                    if args.u is not None and args.u in str(a):
                        fetchAudioMetadata(a)
                else:
                    logging.debug(f"NEW : {a}")
                    nb_fichiers_new += 1
                    fetchAudioMetadata(a, True)


def resultats(start):
    end = time.time() - start
    print("------")
    print(f"{nb_fichiers_existants} fichiers existants")
    print(f"{nb_fichiers_new} nouveaux fichiers")
    print(f"Temps d'exécution : {end} sec.")


if __name__ == "__main__":

    # Arguments
    parser = argparse.ArgumentParser()
    parser.add_argument('-c', help="Cote du fonds à traiter", required=True)
    parser.add_argument('-u', help="Update les fichiers déjà existants", required=False)
    # avec -u on peut mettre la cote d'un document et cela update que ce document. Ou alors si on met une cote id'une unité supérieure (comme celle d'un dossier), cela update celle-ci et tous ces enfants.
    args = parser.parse_args()


    main(args)
